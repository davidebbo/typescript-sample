/// <reference path="../typings/jquery/jquery.d.ts" />

module FooterUpdater {
   export function SendMessageFromTypeScript() {
      var theMessage = "Hello, world!";
      $("#message-from-typescript").text(theMessage);
   }

   SendMessageFromTypeScript();
}
